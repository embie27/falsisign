#!/usr/bin/env python
import tkinter as tk
from sys import argv

TARGET_WIDTH=2480
TARGET_HEIGHT=3508

root = tk.Tk()

signature_img = None
signature_id = None
signature_x, signature_y = (None,None)

# compute window size
w = root.winfo_screenwidth()
h = root.winfo_screenheight()
if w > h:
    canvas_height = int(h * 0.8)
    canvas_width = int(canvas_height * (210 / 297))
else:
    canvas_width = int(w * 0.8)
    canvas_height = int(canvas_width * (297 / 210))

def click(event):
    global signature_id, signature_x,signature_y
    if signature_id is not None:
        canvas.delete(signature_id)
    signature_x = int(event.x/canvas_width*TARGET_WIDTH)
    signature_y = int(event.y/canvas_height*TARGET_HEIGHT)
    if signature_img is not None:
        signature_id = canvas.create_image(event.x, event.y, anchor=tk.NW, image=signature_img)
    else:
        if len(argv) > 1:
            print(f"+{signature_x}+{signature_y}")
        else:
            print(f"-x {signature_x} -y {signature_y}")
        root.destroy()

def submit(event):
    if signature_x is not None and signature_y is not None:
        print(f"+{signature_x}+{signature_y}")
        root.destroy()

canvas = tk.Canvas(root, bg="white", height=canvas_height, width=canvas_width)

if len(argv) > 1:
    img = tk.PhotoImage(file = argv[1])
    zoom = round(img.width() / canvas_width)
    img = img.subsample(zoom)
    canvas.create_image(0, 0, anchor=tk.NW, image=img)

    if len(argv) > 2:
        signature_img = tk.PhotoImage(file = argv[2]).subsample(zoom)


canvas.pack()
canvas.bind("<Button-1>", click)
canvas.bind("<Button-3>", submit)
root.mainloop()
